## l10n_co_edi_jorels

Copyright (2019-2021) - Jorels SAS

[info@jorels.com](mailto:info@jorels.com)

[https://www.jorels.com](https://www.jorels.com)

Under LGPL (Lesser General Public License)

# Contributing

Jorge Sanabria (2019-2021) - [js@jorels.com](mailto:js@jorels.com)

Leonardo Martinez (2019-2020) -
[lotharius96@protonmail.ch](mailto:lotharius96@protonmail.ch)

Joseph Armas (2021-2022) -
[josepharmas@adsecuador.com](mailto:josepharmas@adsecuador.com)
